package main

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"time"
)

var users []User

const (
	DEFAULT_TIMEFORMAT = "2006-01-02 15:04:05"
)

func CreateUserHandler(w http.ResponseWriter, req *http.Request) {
	id := uuid.New()
	enc := json.NewEncoder(w)
	var user User
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		encErr := enc.Encode(&err)
		logrus.Error(encErr)
	}
	user.ID = &id
	users = append(users, user)
	w.WriteHeader(http.StatusCreated)
	encErr := enc.Encode(&user)
	if encErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		enc.Encode(&encErr)
		logrus.Error(encErr)
	}
}

func ListUsersHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)

	encErr := enc.Encode(&users)
	if encErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error(encErr)
	}
}

func UpdateUserHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)
	user, err := getUser(vars["id"])
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		encErr := enc.Encode(&err)
		logrus.Error(encErr)
		return
	}

	var patch User
	err = json.NewDecoder(req.Body).Decode(&patch)
	patch.ID = user.ID
	user, err = replaceUser(patch)

	w.WriteHeader(http.StatusOK)
	encErr := enc.Encode(&user)
	if encErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		enc.Encode(&encErr)
		logrus.Error(encErr)
	}
}

func GetUserHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)
	user, err := getUser(vars["id"])
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(&err)
	}
	w.WriteHeader(http.StatusOK)
	encErr := enc.Encode(&user)
	if encErr != nil {
		logrus.Error(encErr)
		w.WriteHeader(http.StatusInternalServerError)
		enc.Encode(&encErr)
	}
}

func parseTaskPayload(body io.Reader) (*Task, error) {
	var task Task
	var data map[string]string
	err := json.NewDecoder(body).Decode(&data)
	if err != nil {
		return nil, err
	}
	id, err := uuid.Parse(data["id"])
	if err != nil && data["id"] != "" {
		return nil, err
	}
	if data["id"] != "" {
		task.ID = &id
	}
	task.Name = data["name"]
	task.Description = data["description"]

	t, err := time.Parse(DEFAULT_TIMEFORMAT, data["date_time"])
	if err != nil {
		return nil, err
	}
	task.DateTime = &t
	return &task, nil
}

func CreateUserTaskHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)

	user, err := getUser(vars["user_id"])
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(&err)
		return
	}

	id := uuid.New()
	t, err := parseTaskPayload(req.Body)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		enc.Encode(&err)
		return
	}
	task := *t
	task.ID = &id
	user.tasks = append(user.tasks, task)
	_, err = replaceUser(*user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		enc.Encode(err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	enc.Encode(&task)
}

func DeleteUserTaskHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)

	vars := mux.Vars(req)
	userId := vars["user_id"]
	taskId := vars["task_id"]

	user, err := getUser(userId)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(err)
		return
	}

	ok, err := removeTask(user, taskId)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(&err)
		return
	}
	if ok {
		w.WriteHeader(http.StatusOK)
		return
	}
}

func removeTask(user *User, taskId string) (bool, error) {
	tasks := user.tasks
	if !hasTask(taskId, tasks) {
		return false, errors.New("task not found")
	}
	user.tasks = []Task{}
	for _, task := range tasks {
		if task.ID.String() != taskId {
			user.tasks = append(user.tasks, task)
		}
	}
	_, err := replaceUser(*user)
	if err != nil {
		return false, err
	}
	return true, nil
}

func hasTask(id string, taskList []Task) bool {
	for _, task := range taskList {
		if task.ID.String() == id {
			return true
		}
	}
	return false
}

func UpdateUserTaskHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)
	user, err := getUser(vars["user_id"])
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(&err)
		return
	}
	t, err := parseTaskPayload(req.Body)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		enc.Encode(&err)
		return
	}
	patch := *t
	task_id, err := uuid.Parse(vars["task_id"])
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusBadRequest)
		enc.Encode(&err)
		return
	}
	patch.ID = &task_id
	tsk, err := replaceTask(*user, patch)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		enc.Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	enc.Encode(&tsk)
}

func GetUserTaskListHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)
	user, err := getUser(vars["user_id"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	encErr := enc.Encode(user.tasks)
	if encErr != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func replaceTask(user User, task Task) (*Task, error) {
	for i, t := range user.tasks {
		if t.ID.String() == task.ID.String() {
			user.tasks[i] = task
			return &task, nil
		}
	}
	return nil, errors.New("unable to find task for user")
}

func replaceUser(patch User) (*User, error) {
	for i, user := range users {
		if user.ID.String() == patch.ID.String() {
			users[i] = patch
			return &patch, nil
		}
	}
	return nil, errors.New("not able to find user to update")
}

func getUser(id string) (*User, error) {
	for _, user := range users {
		if user.ID.String() == id {
			return &user, nil
		}
	}
	return nil, errors.New("unable to find user")
}
