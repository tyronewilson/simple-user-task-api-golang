package main

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)

	sig := make(chan os.Signal)

	signal.Notify(sig, os.Interrupt, os.Kill)

	go func() {
		router := Router()
		printRoutingTable(router, os.Stdout)
		addr := "localhost:8080"
		logrus.Infof("Serving on %s", addr)
		corsHandler := cors.New(cors.Options{
			AllowedOrigins: []string{
				"http://localhost:4200", //angular app for example
			},
			AllowCredentials: true,
			Debug:            true,
			AllowedMethods:   []string{"GET", "PUT", "POST", "DELETE"},
		}).Handler(router)

		srv := &http.Server{
			Addr:    addr,
			Handler: corsHandler,
		}
		logrus.Fatal(srv.ListenAndServe())

	}()

	s := <-sig

	logrus.Infof("Shut down signal received %s", s)

	logrus.Info("Done.")
}

func Router() *mux.Router {
	router := mux.NewRouter()

	// User endpoints
	router.HandleFunc("/api/user", CreateUserHandler).Methods("POST")
	router.HandleFunc("/api/user/{id}", UpdateUserHandler).Methods("PUT")
	router.HandleFunc("/api/user", ListUsersHandler).Methods("GET")
	router.HandleFunc("/api/user/{id}", GetUserHandler).Methods("GET")

	// Task endpoints
	router.HandleFunc("/api/user/{user_id}/task", CreateUserTaskHandler).Methods("POST")
	router.HandleFunc("/api/user/{user_id}/task/{task_id}", UpdateUserTaskHandler).Methods("PUT")
	router.HandleFunc("/api/user/{user_id}/task/{task_id}", DeleteUserTaskHandler).Methods("DELETE")
	router.HandleFunc("/api/user/{user_id}/task", GetUserTaskListHandler).Methods("GET")
	router.HandleFunc("/api/user/{user_id}/task/{task_id}", GetUserTaskHandler).Methods("GET")

	return router
}

func GetUserTaskHandler(w http.ResponseWriter, req *http.Request) {
	enc := json.NewEncoder(w)
	vars := mux.Vars(req)
	userId := vars["user_id"]
	user, err := getUser(userId)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	task, err := getUserTask(*user,vars["task_id"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	enc.Encode(task)
}

func getUserTask(user User, taskId string) (*Task, error) {
	for _, task := range user.tasks {
		if task.ID.String() == taskId {
			return &task, nil
		}
	}
	return nil, errors.New("task not found")
}

