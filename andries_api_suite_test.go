package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestAndriesApi(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Andries Api Suite")
}


func POST(url string, body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	return testEndpoint("POST", url, body)
}

func PUT(url string, body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	return testEndpoint("PUT", url, body)
}

func DELETE(url string, body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	return testEndpoint("DELETE", url, body)
}

func GET(url string, body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	return testEndpoint("GET", url, body)
}

func testEndpoint(method string, url string, body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	req, _ := http.NewRequest(method, url, body)
	res := httptest.NewRecorder()
	Router().ServeHTTP(res, req)
	return res, req
}

func assertNotFound(res *httptest.ResponseRecorder) {
	Expect(res.Code).To(Equal(http.StatusNotFound))
}

func assertOK(res *httptest.ResponseRecorder) {
	Expect(res.Code).To(Equal(http.StatusOK))
}

func assertCreated(res *httptest.ResponseRecorder) {
	Expect(res.Code).To(Equal(http.StatusCreated))
}

func assertBadRequest(res *httptest.ResponseRecorder) {
	Expect(res.Code).To(Equal(http.StatusBadRequest))
}