package main

import (
	"github.com/google/uuid"
	"time"
)

type Task struct {
	ID *uuid.UUID `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	DateTime *time.Time `json:"date_time"`
}

type User struct {
	ID        *uuid.UUID `json:"id"`
	Username  string     `json:"username"`
	FirstName string     `json:"first_name"`
	LastName  string     `json:"last_name"`
	tasks []Task `json:"tasks"`
}
