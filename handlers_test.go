package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strings"
	"time"
)

var userPayload = `{"username":"jsmith","first_name" : "John", "last_name" : "Smith"}`
var taskPayload = `{"name":"My task","description" : "Description of task", "date_time" : "2016-05-25 14:25:00"}`

var _ = Describe("Handlers", func() {
	Describe("CreateUserHandler", func() {
		It("adds a new user when the data is valid", func() {
			res, _ := POST("/api/user", strings.NewReader(userPayload))
			assertCreated(res)
			Expect(users).To(HaveLen(1))
		})

		Context("with invalid data", func() {
			It("rejects an empty body with 422", func() {
				data := ""
				res, _ := POST("/api/user", strings.NewReader(data))
				assertBadRequest(res)
			})
		})
	})

	Describe("UpdateUserHandler", func() {
		Context("when the user does not exist", func() {
			It("results in StatusNotfound", func() {
				id := uuid.New()

				url := "/api/user/" + id.String()
				res, _ := PUT(url, strings.NewReader(userPayload))
				assertNotFound(res)
			})

		})
		Context("when the user exists", func() {
			It("updates the user", func() {
				id := uuid.New()
				user := User{
					ID:        &id,
					Username:  "test",
					FirstName: "test",
					LastName:  "test",
				}
				users = append(users, user)
				res, _ := PUT("/api/user/"+id.String(), strings.NewReader(userPayload))
				assertOK(res)
			})
		})
	})

	Describe("GetUserHandler", func() {
		Context("when the user does not exist", func() {
			It("results in 404", func() {
				id := uuid.New()
				url := "/api/user/" + id.String()
				res, _ := GET(url, nil)
				assertNotFound(res)
			})
		})

		Context("when the user does exist", func() {
			It("results in 200 with correct data", func() {
				id := uuid.New()
				user := User{
					ID:        &id,
					Username:  "test-name",
					FirstName: "Test",
					LastName:  "Name",
				}
				users = append(users, user)
				url := "/api/user/" + id.String()
				res, _ := GET(url, nil)
				assertOK(res)
				var data User
				err := json.NewDecoder(res.Body).Decode(&data)
				Expect(err).To(BeNil())
				Expect(data.ID.String()).To(Equal(id.String()))
				Expect(data.Username).To(Equal(user.Username))
				Expect(data.FirstName).To(Equal(user.FirstName))
				Expect(data.LastName).To(Equal(user.LastName))
			})
		})
	})

	Describe("ListUsersHandler", func() {
		Context("when there are no users", func() {
			It("returns an empty list", func() {
				users = []User{}
				url := "/api/user"
				res, _ := GET(url, nil)
				assertOK(res)
				var data []User
				err := json.NewDecoder(res.Body).Decode(&data)
				Expect(err).To(BeNil())
				Expect(data).To(HaveLen(0))
			})
		})

		Context("When there are users present", func() {
			It("returns the users", func() {
				users = []User{
					createUser(),
					createUser(),
					createUser(),
				}
				url := "/api/user"
				res, _ := GET(url, nil)
				assertOK(res)

				var data []User
				err := json.NewDecoder(res.Body).Decode(&data)
				Expect(err).To(BeNil())
				Expect(data).To(HaveLen(3))
				Expect(data).To(Equal(users))
			})
		})
	})

	Describe("CreateUserTaskHandler", func() {
		Context("when the user doesn't exist", func() {
			It("returns 404", func() {
				id := uuid.New()
				url := "/api/user/" + id.String() + "/task"
				res, _ := POST(url, strings.NewReader(taskPayload))
				assertNotFound(res)
			})
		})

		Context("when the user exists", func() {
			It("Adds the task", func() {
				user := createUser()
				users = append(users, user)
				url := fmt.Sprintf("/api/user/%s/task", user.ID.String())
				res, _ := POST(url, strings.NewReader(taskPayload))
				assertCreated(res)
				savedUser, err := getUser(user.ID.String())
				Expect(err).To(BeNil())
				Expect(savedUser.tasks).To(HaveLen(1))

			})
		})
	})

	Describe("UpdateUserTaskHandler", func() {
		Context("when the user is not found", func() {
			It("returns 404", func() {
				id := uuid.New()
				url := fmt.Sprintf("/api/user/%s/task/%s", id.String(), uuid.New().String())
				res, _ := PUT(url, strings.NewReader(taskPayload))
				assertNotFound(res)
			})
		})

		Context("when the user exists", func() {
			Context("but the task does not", func() {
				It("response with 404", func() {
					user := createUser()
					users = append(users, user)
					url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), uuid.New().String())
					res, _ := PUT(url, strings.NewReader(taskPayload))
					assertNotFound(res)
				})
			})

			Context("when the task is present", func() {
				It("responds OK", func() {
					user := createUser()
					taskId := uuid.New()
					t := time.Now()
					user.tasks = append(user.tasks, Task{
						ID:          &taskId,
						Name:        "Some task",
						Description: "With a description",
						DateTime:    &t,
					})
					users = append(users, user)
					url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), taskId.String())
					res, _ := PUT(url, strings.NewReader(taskPayload))
					assertOK(res)

					savedUser, err := getUser(user.ID.String())
					Expect(err).To(BeNil())
					Expect(savedUser.tasks).To(HaveLen(1))
					Expect(savedUser.tasks[0].Name).To(Equal("My task"))
				})
			})
		})
	})

	Describe("DeleteUserTaskHandler", func() {
		Context("when the user doesn't exist", func() {
			It("responds with 404", func() {
				userId := uuid.New()
				taskId := uuid.New()
				url := fmt.Sprintf("/api/user/%s/task/%s", userId.String(), taskId.String())
				res, _ := DELETE(url, nil)
				assertNotFound(res)
			})
		})
		Context("when the task id is not present", func() {
			It("responds with 404", func() {
				user := createUser()
				users = append(users, user)
				url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), uuid.New().String())
				res, _ := DELETE(url, nil)
				assertNotFound(res)
			})
		})
		Context("when the user and task exist", func() {
			It("response with OK", func() {
				user := createUser()
				taskId := uuid.New()
				task := Task{
					ID:          &taskId,
					Name:        "Some task",
					Description: "Bla",
				}
				user.tasks = append(user.tasks, task)
				users = append(users, user)
				url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), taskId.String())
				res, _ := DELETE(url, nil)
				assertOK(res)

				savedUser, err := getUser(user.ID.String())
				Expect(err).To(BeNil())
				Expect(savedUser.tasks).To(HaveLen(0))
			})
		})
	})

	Describe("replaceTask", func() {
		It("replaces the task", func() {
			user := createUser()
			taskId := uuid.New()
			task := Task{
				ID:          &taskId,
				Name:        "Test",
				Description: "Task",
			}
			user.tasks = []Task{task}
			replacement := Task{
				ID:          &taskId,
				Name:        "Updated",
				Description: "Task",
			}
			_, err := replaceTask(user, replacement)
			Expect(err).To(BeNil())
			Expect(user.tasks).To(HaveLen(1))
			Expect(user.tasks[0].Name).To(Equal("Updated"))
		})
	})

	Describe("GetUserTaskListHandler", func() {
		Context("when the user does not exist", func() {
			It("responds with 404", func() {
				userId := uuid.New()
				url := fmt.Sprintf("/api/user/%s/task", userId.String())
				res, _ := GET(url, nil)
				assertNotFound(res)
			})
		})
		Context("when the user is exists", func() {
			It("response with OK and correct data", func() {
				user := createUser()
				taskId := uuid.New()
				user.tasks = []Task{
					{
						ID:          &taskId,
						Name:        "My task",
						Description: "Testing this task thing",
					},
				}
				users = append(users, user)

				url := fmt.Sprintf("/api/user/%s/task", user.ID.String())
				res, _ := GET(url, nil)
				assertOK(res)
				var tasks []Task
				err := json.NewDecoder(res.Body).Decode(&tasks)
				Expect(err).To(BeNil())
				Expect(tasks).To(HaveLen(1))
				Expect(tasks[0].ID.String()).To(Equal(taskId.String()))
			})
		})
	})

	Describe("GetUserTaskHandler", func() {
		Context("when the user does not exist", func() {
			It("responds with 404", func() {
				url := fmt.Sprintf("/api/user/%s/task/%s", uuid.New().String(), uuid.New().String())
				res, _ := GET(url, nil)
				assertNotFound(res)
			})
		})
		Context("when the task does not exist", func() {
			It("returns 404", func() {
				user := createUser()
				users = append(users, user)
				url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), uuid.New().String())
				res, _ := GET(url, nil)
				assertNotFound(res)
			})
		})

		Context("when the task exists", func() {
			It("returns OK with the correct data", func() {
				user := createUser()
				task1 := createTask()
				task2 := createTask()
				task2.Name = "Task 2"
				task1.Name = "Task 1"
				user.tasks = []Task{task1, task2}
				users = append(users, user)

				url := fmt.Sprintf("/api/user/%s/task/%s", user.ID.String(), task1.ID.String())

				res, _ := GET(url, nil)
				assertOK(res)

				var data Task
				err := json.NewDecoder(res.Body).Decode(&data)
				Expect(err).To(BeNil())

				Expect(data.ID.String()).To(Equal(task1.ID.String()))
				Expect(data.Name).To(Equal(task1.Name))
			})
		})
	})
})

func createUser() User {
	id := uuid.New()
	return User{
		ID: &id,
	}
}

func createTask() Task {
	id := uuid.New()
	t := time.Now()
	return Task{
		ID:          &id,
		Name:        "Some task",
		Description: "description",
		DateTime:    &t,
	}
}
