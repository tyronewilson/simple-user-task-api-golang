package main

import (
	"github.com/gorilla/mux"
	"github.com/kataras/tablewriter"
	"io"
	"strings"
)

func printRoutingTable(router *mux.Router, writer io.Writer) {
	table := tablewriter.NewWriter(writer)
	table.SetHeader([]string{
		"PATH",
		"REGEXP",
		"METHODS",
		"QUERY TEMPLATES",
		"QUERY REGEXPS",
	})

	router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		var row []string
		pathTemplate, err := route.GetPathTemplate()
		if err == nil {
			row = append(row, pathTemplate)
		}
		pathRegexp, err := route.GetPathRegexp()
		if err == nil {
			row = append(row, pathRegexp)
		}
		methods, err := route.GetMethods()
		if err == nil {
			row = append(row, strings.Join(methods, ", "))
		}
		if len(methods) == 0 {
			row = append(row, "")
		}
		queriesTemplates, err := route.GetQueriesTemplates()
		if err == nil {
			row = append(row, strings.Join(queriesTemplates, ","))
		}
		if len(queriesTemplates) == 0 {
			row = append(row, "")
		}
		queriesRegexps, err := route.GetQueriesRegexp()
		if err == nil {
			row = append(row, strings.Join(queriesRegexps, ","))
		}
		if len(queriesTemplates) == 0 {
			row = append(row, "")
		}
		table.Append(row)
		return nil
	})

	table.Render()
}
